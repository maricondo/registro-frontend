
const package = require('./package.json')
module.exports = {
  transpileDependencies: ["vuetify"],
  publicPath: '//localhost:8081',
  devServer: {
    port: 8081,
    historyApiFallback: true,
  },
  configureWebpack: {
    output: {
      library: package.name,
      libraryTarget: 'umd'
    },
  },
  chainWebpack: (config) => {
    config.module.rules.delete("eslint");
  },
};