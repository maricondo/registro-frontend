import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueToastr2 from "vue-toastr-2";
import "vue-toastr-2/dist/vue-toastr-2.min.css";
import singleSpaVue from 'single-spa-vue'

window.toastr = require("toastr");
var toastrConfigs = {
  positionClass: "toast-bottom-center",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "0",
};
Vue.use(VueToastr2, toastrConfigs);

//moment
Vue.use(require("vue-moment"));

Vue.config.productionTip = false;

// new Vue({
//   router,
//   store,
//   vuetify,
//   render: (h) => h(App),
// }).$mount("#app");


const appOptions = {
  el: '#microApp',
  router,
  store,
  vuetify,
  render: h => h(App)
}



if (!window.singleSpaNavigate) {
  delete appOptions.el
  new Vue(appOptions).$mount('#app')
}

const vueLifecycle = singleSpaVue({
  Vue,
  appOptions
})

export function bootstrap () {
  console.log('memoriales bootstrap')
  return vueLifecycle.bootstrap(() => {})
}

export function mount (props) {
  console.log('memoriales mount')
  // console.log(parseJwt(props.authToken)+" asd "+props.ejemplo);
  return vueLifecycle.mount(() => {props})
}

export function unmount () {
  console.log('memoriales unmount')
  return vueLifecycle.unmount(() => {})
}