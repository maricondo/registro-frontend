import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import es from "vuetify/es5/locale/es";

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { es },
    current: "es",
  },
  theme: {
    themes: {
      light: {
        primary: "#1976D2",
        secondary: "#455A64",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#039BE5",
        success: "#4CAF50",
        warning: "#FFC107",
        fondo: "#F4F8F9",
        headerTabla: "#f2f3f4",
      },
    },
  },
});
