import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },

  /*
  {
    path: "/juzgados",
    name: "juzgados",
    component: () => import("../views/juzgados/index"),
  },
  {
    path: "/bajas-juzgados",
    name: "bajas-juzgados",
    component: () => import("../views/bajasJuzgados/index"),
  },
  {
    path: "/turnos-juzgados",
    name: "turnos-juzgados",
    component: () => import("../views/turnosJuzgados/index"),
  },

  {
    path: "/departamentos",
    name: "departamentos",
    component: () => import("../views/departamentos/index"),
  },
  {
    path: "/provincias",
    name: "provincias",
    component: () => import("../views/provincias/index"),
  },
  {
    path: "/municipios",
    name: "municipios",
    component: () => import("../views/municipios/index"),
  },

  {
    path: "/areas-organizacionales",
    name: "areas-organizacionales",
    component: () => import("../views/areasOrganizacionales/index"),
  },

  {
    path: "/entes",
    name: "entes",
    component: () => import("../views/entes/index"),
  },
  {
    path: "/escalas-salariales",
    name: "escalas-salariales",
    component: () => import("../views/escalasSalariales/index"),
  },

  {
    path: "/planillas-presupuestarias",
    name: "planillas-presupuestarias",
    component: () => import("../views/planillasPresupuestarias/index"),
  },
  {
    path: "/oficinas",
    name: "oficinas",
    component: () => import("../views/oficinas/index"),
  },

  {
    path: "/bajas-juzgados/:id_baja_juzgado/archivo",
    name: "bajas-juzgados.archivo",
    component: () => import("../views/bajasJuzgados/respaldo"),
  },

  {
    path: "/instancias",
    name: "instancias",
    component: () => import("../views/instancias/index"),
  },
  {
    path: "/oficinasInstancias",
    name: "oficinasInstancias",
    component: () => import("../components/molecules/oficinas/COficinaInstancia"),
  },
  {
    path: "/juzgados-form",
    name: "juzgados-form",
    component: () => import("../views/juzgados/form"),
  },
  {
    path: "/zonas",
    name: "zonas",
    component: () => import("../views/zonas/index"),
  },
  {
    path: "/gestoras",
    name: "gestoras",
    component: () => import("../views/oficinas/gestoras"),
  },
  {
    path: "/gestoras-competencias",
    name: "gestoras-competencias",
    component: () => import("../views/oficinas/competencias"),
  },*/
  {
    path: "/registro-notas",
    name: "registroNotas",
    component: () => import("../views/registroNotas/index"),
  },
  {
    path: "/consulta-registros",
    name: "consultaRegistros",
    component: () => import("../views/registroNotas/indexConsultas"),
  },
];

const router = new VueRouter({
  mode: "history",
  // base: process.env.BASE_URL,
  base: process.env.VUE_APP_BASE_URL,
  routes,
});

export default router;
