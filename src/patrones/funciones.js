export const funciones = {
  validar(e, patron) {
    const tecla = e.keyCode || e.which;

    if (tecla === 8 || tecla === 9) {
      return true;
    }

    const tecla_final = String.fromCharCode(tecla);
    if (patron.test(tecla_final)) return true;
    else e.preventDefault();
  },
  soloNumero(e) {
    const patron = /[0-9]/;
    return this.validar(e, patron);
  },
};
