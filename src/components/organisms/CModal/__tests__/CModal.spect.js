import { shallowMount } from "@vue/test-utils";
import CModal from "../CModal";
describe("Componente CModal", () => {
  it("Recive en la variable valor=true", () => {
    const valor = true;
    const wrapper = shallowMount(CModal, {
      propsData: {
        valor: valor,
      },
    });
    expect(wrapper.props().valor).toBeTruthy();
  });

  it("Recibe en la variable valor=false", () => {
    const valor = false;
    const wrapper = shallowMount(CModal, {
      propsData: {
        valor: valor,
      },
    });
    expect(wrapper.props().valor).toBeFalsy();
  });
  it("Pasa el titulo", () => {
    const titulo = "Titulo del Modal";
    const wrapper = shallowMount(CModal, {
      propsData: {
        titulo: titulo,
      },
    });
    expect(wrapper.props().titulo).toContain(titulo);
  });
  it("Funciona slot default", () => {
    const wrapper = shallowMount(CModal, {
      slots: {
        default: '<div class="slot"></div>',
      },
    });

    expect(wrapper.find(".slot")).toBeTruthy();
  });
  it("Funciona slot actions", () => {
    const wrapper = shallowMount(CModal, {
      slots: {
        actions: '<div class="action"></div>',
      },
    });

    expect(wrapper.find(".action")).toBeTruthy();
  });
  it("Instantanea de CModal", () => {});
});
