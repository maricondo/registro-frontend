import { shallowMount } from "@vue/test-utils";
import CBuscar from "../CBuscar";

describe("Componente CBuscar", () => {
  it("Instantane de CBuscar", () => {
    const wrapper = shallowMount(CBuscar);
    expect(wrapper).toMatchSnapshot();
  });

  it("Muestra placeholder", () => {
    const textoPlaceholder = "texto placeholder";
    const wrapper = shallowMount(CBuscar, {
      propsData: {
        placeholder: textoPlaceholder,
      },
    });
    expect(wrapper.html()).toContain(textoPlaceholder);
  });

  it("Emitir parametros de busqueda", async () => {
    const wrapper = shallowMount(CBuscar);
    wrapper.vm.$emit("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("click")).toBeTruthy();
  });
});
