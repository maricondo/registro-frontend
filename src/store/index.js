import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    urlBase: `${process.env.VUE_APP_URL}`,
    tituloModulo: "",
    categoriasEscalas: ["Operativo", "Superior", "Ejecutivo"],
    urlPublica: "http://127.0.0.1:3000/"
  },
  mutations: {},
  actions: {},
  modules: {},

});
